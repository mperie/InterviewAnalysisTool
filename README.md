# InterviewAnalysisTool

## Installation
### Windows
1. Activate vitualization
2. Install WSL
3. Install Docker for Windows
4. Set memory usage of WSL's VM: [link](https://learn.microsoft.com/en-us/windows/wsl/wsl-config#configure-global-options-with-wslconfig)

For remote expe:
1. Docker for Windows -> options -> resources -> WSL -> activate for the right distro
2. test `docker run hello-world` on WSL
3. Create ~/.ssh folder with config and keys
4. Start ssh-agent att boot: 
```
SSH_ENV="$HOME/.ssh/agent-environment"

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add;
}

# Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    #ps ${SSH_AGENT_PID} doesn't work under cywgin
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi
```
5. ssh-add the key
6. start the project, ssh ia-server and do:
    A. `chown root /root/.ssh/ -R`
    B. `chmod 700 /root/.ssh; chmod 600 /root/.ssh/*`