#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/root/miniconda3/lib/ >> /home/user/interview_analysis_tool/out.log 2>> /home/user/interview_analysis_tool/error.log;

PIDS=""
PID=$(/home/user/miniconda3/envs/TheEnv/bin/python3 /home/user/interview_analysis_tool/transcription/run_transcription.py ${@:1} >> task_logs.log & echo $!)
if [[ $PIDS != "" ]]
then
   PIDS+=" "
fi
PIDS+="${PID}"
echo "[RUN_ID_start]$PIDS[RUN_ID_stop]"
