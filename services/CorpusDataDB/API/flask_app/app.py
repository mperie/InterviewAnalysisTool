from flask import Flask, request, jsonify, make_response, send_file
from flask_cors import CORS
from pymongo import MongoClient
from gridfs import GridFS
from bson.objectid import ObjectId
import json
from utils import CorpusDataDBConfig


config = CorpusDataDBConfig()
app = Flask(__name__)
CORS(app)
client = MongoClient('mongodb://{}:{}/'.format(config.MONGODB_HOST, config.MONGODB_PORT), username=config.MONGODB_USERNAME, password=config.MONGODB_PASSWORD)
db = client.main_db
fs = GridFS(db)

# Return request with a document matching filter 
def _get_one_by_filter(collection, filter, query=None, sub_doc=None):
    element = collection.find_one(filter, query)
    if sub_doc != None:
        element = element[sub_doc]
    if element:
        if '_id' in element:
            element['_id'] = str(element['_id'])
        return make_response(jsonify(element), 200)
    else:
        return make_response("No element found matching {} in collection {}".format(filter, collection), 403)

# Return request with multiple documents matching filter 
def _get_multiple_by_filter(collection, filter, query=None):
    elements = []
    for element in collection.find(filter, query):
        element['_id'] = str(element['_id'])
        elements.append(element)
    return make_response(jsonify(elements), 200)

# Return request with a file from a document 
def _get_file_from_document(document, filename):
    if not filename in document: 
        return make_response("No file {} found with in document with id {}".format(filename, document['_id']), 403)
    file = fs.get(ObjectId(document[filename]))
    return send_file(file, attachment_filename=file.filename)


# Add filtes to a document
def _add_files_to_document(collection, document, files):
    # Add files in GridFS
    updates_to_do = {'$set': {}}
    for file_name in files:
        file_id = fs.put(files[file_name], filename=files[file_name].filename)
        updates_to_do['$set'][file_name] = str(file_id)
    # Add GridFS ids and filenames to document
    collection.update_one({"_id": document["_id"]}, updates_to_do)
    return make_response(jsonify({'success':True}), 200)

def _add_files_in_list_in_document(collection, document, files, file_type):
    # Add files in GridFS
    updates_to_do = {'$set': {}}
    for file_name in files:
        file_id = fs.put(files[file_name], filename=files[file_name].filename)
        try:
            filename, extension = (files[file_name].filename).split(".")
        except:
            filename, extension = (files[file_name].filename).split(" ")
        updates_to_do['$set'][file_type+"."+filename+" "+extension] = str(file_id)
    # Add GridFS ids and filenames to document
    collection.update_one({"_id": document["_id"]}, updates_to_do)
    return make_response(jsonify({'success':True}), 200)

def _remove_file_from_document(collection, document, file_type, filename):
    if not filename in document[file_type]: 
        return make_response("No file {} found with in {} in document with id {}".format(filename, file_type, document['_id']), 403)
    fs.delete(ObjectId(document[file_type][filename]))
    # response = collection.update_one({ '_id': document['_id'] }, { '$unset' : { filename : "638a39647fc1c8c1d337bf3d"} })
    response = collection.update_one({"_id": document["_id"]}, {'$unset': {'{}.{}'.format(file_type, filename):1}})
    return make_response(jsonify({'success':True}), 200)


#################################
########## CORPUS ##########
# GET
@app.route('/corpus', methods=['GET']) # list corpus given filter
def list_corpus():
    filter = request.args.get('filter', default=None)
    if filter is not None:
        query = request.args.get('query', default=None)
        return _get_multiple_by_filter(db.corpus, json.loads(filter), json.loads(query) if query else None)
    else:
        return make_response("You must provide a filter in the request args", 403)

@app.route('/corpus/light', methods=['GET']) # list corpus given filter
def list_all_light_corpus():
    filter = request.args.get('filter', default=None)
    if filter is not None:
        query = '{ "name": 1, "created_on":1, "description":1 }'
        return _get_multiple_by_filter(db.corpus, json.loads(filter), json.loads(query) if query else None)
    else:
        return make_response("You must provide a filter in the request args", 403)

@app.route('/corpus/<id>', methods=['GET']) # get a corpus by its id
def get_corpus_by_id(id):
    return _get_one_by_filter(db.corpus, {"_id": ObjectId(id)})

@app.route('/corpus/<id>/<file>', methods=['GET']) # get file from a corpus by its name
def get_corpus_file(id, file):
    corpus = db.corpus.find_one({"_id": ObjectId(id)})
    if corpus:
        return _get_file_from_document(corpus, file)
    else:
        return make_response("No corpus found with id {}".format(id), 403)

@app.route('/corpus/<id>/sub/<sub_doc>', methods=['GET']) # get a corpus by its id
def get_corpus_sub_doc_by_id(id, sub_doc):
    return _get_one_by_filter(db.corpus, {"_id": ObjectId(id)}, sub_doc=sub_doc)

@app.route('/corpus/<id>/<sub_doc>/<file>', methods=['GET']) # get file from a corpus by its name
def get_corpus_sub_file(id, sub_doc, file):
    corpus = db.corpus.find_one({"_id": ObjectId(id)})
    if corpus:
        return _get_file_from_document(corpus[sub_doc], file)
    else:
        return make_response("No corpus found with id {}".format(id), 403)

# POST
@app.route('/corpus', methods=['POST']) # add a corpus
def create_corpus():
    added_corpus_id = db.corpus.insert_one(request.json).inserted_id
    return make_response(jsonify({"ID": str(added_corpus_id)}), 200)

@app.route('/corpus/<id>/files', methods=['POST']) # add files to a corpus
def add_corpus_files(id):
    corpus = db.corpus.find_one({"_id": ObjectId(id)})
    if corpus:
        return _add_files_to_document(db.corpus, corpus, request.files)
    else:
        return make_response("No corpus found with id {}".format(id), 403)

# @app.route('/corpus/<id>/media_files', methods=['POST'])
# def add_media_file(id):
#     corpus = db.corpus.find_one({"_id": ObjectId(id)})
#     if corpus:
#         return _add_files_in_list_in_document(db.corpus, corpus, request.files, "media_files")
#     else:
#         return make_response("No corpus found with id {}".format(id), 403)

@app.route('/corpus/<id>/<sub_files>', methods=['POST'])
def add_media_file(id, sub_files):
    corpus = db.corpus.find_one({"_id": ObjectId(id)})
    if corpus:
        return _add_files_in_list_in_document(db.corpus, corpus, request.files, sub_files)
    else:
        return make_response("No corpus found with id {}".format(id), 403)


# PATCH
@app.route('/corpus/<id>', methods=['PATCH']) # PATCH data
def patch_corpus_by_id(id):
    update_data = db.corpus.find_one_and_update({"_id" : ObjectId(id)}, {"$set": request.get_json()})
    # if update_data == None:
    #     update_data = db.data_saves
    return make_response(jsonify({"data updated": str(update_data)}), 200)

# DELETE
@app.route('/corpus/<id>', methods=['DELETE']) # remove a corpus by its id
def delete_corpus_by_id(id):
    db.corpus.delete_one({"_id": ObjectId(id)})
    return make_response(jsonify({'success':True}), 200)

@app.route('/corpus/<id>/<sub_doc>/<file_name>', methods=['DELETE'])
def delete_subfile(id, sub_doc, file_name):
    corpus = db.corpus.find_one({"_id": ObjectId(id)})
    if corpus:
        return _remove_file_from_document(db.corpus, corpus, sub_doc, file_name)
    else:
        return make_response("No corpus found with id {}".format(id), 403)

@app.route('/corpus/', methods=['DELETE']) # remove multiple corpus given a checkpoint id
def delete_corpus():
    checkpoint_id = int(request.args.get('checkpoint_id', default=None))
    if checkpoint_id is not None:
        db.corpus.delete_many({"checkpoint_id": checkpoint_id})
        return make_response(jsonify({'success':True}), 200)
    else:
        return make_response("You must provide a checkpoint_id in the request args", 403)


######################################
########## DATA SAVES ##########
# GET
@app.route('/data_saves', methods=['GET']) # list data saves given filter
def list_data_saves():
    filter = request.args.get('filter', default=None)
    if filter is not None:
        query = request.args.get('query', default=None)
        return _get_multiple_by_filter(db.data_saves, json.loads(filter), json.loads(query) if query else None)
    else:
        return make_response("You must provide a filter in the request args", 403)

@app.route('/data_saves/<id>', methods=['GET']) # get a data save by its id
def get_data_save_by_id(id):
    return _get_one_by_filter(db.data_saves, {"_id": ObjectId(id)})

@app.route('/data_saves/<id>/<file>', methods=['GET']) # get file from a data save by its name
def get_data_save_file(id, file):
    data_save = db.data_saves.find_one({"_id": ObjectId(id)})
    if data_save:
        return _get_file_from_document(data_save, file)
    else:
        return make_response("No data_save found with id {}".format(id), 403)

# POST
@app.route('/data_saves', methods=['POST']) # add a data save
def create_data_save():
    added_data_save_id = db.data_saves.insert_one(request.json).inserted_id
    return make_response(jsonify({"ID": str(added_data_save_id)}), 200)

@app.route('/data_saves/<id>/files', methods=['POST']) # add files to a data save
def add_data_save_files(id):
    data_save = db.data_saves.find_one({"_id": ObjectId(id)})
    if data_save:
        return _add_files_to_document(db.data_saves, data_save, request.files)
    else:
        return make_response("No data_save found with id {}".format(id), 403)
# PATCH
@app.route('/data_saves/<id>', methods=['PATCH']) # PATCH data
def patch_data_by_id(id):
    update_data = db.data_saves.find_one_and_update({"_id" : ObjectId(id)}, {"$set": request.get_json()})
    return make_response(jsonify({"data updated": str(update_data)}), 200)

# DELETE
@app.route('/data_saves/<id>', methods=['DELETE']) # remove a data save by its id
def delete_data_save_by_id(id):
    db.data_saves.delete_one({"_id": ObjectId(id)})
    return make_response(jsonify({'success':True}), 200)

@app.route('/data_saves/', methods=['DELETE']) # remove multiple data save given a data id
def delete_data_saves():
    data_id = int(request.args.get('data_id', default=None))
    if data_id is not None:
        db.data_saves.delete_many({"data_id": data_id})
        return make_response(jsonify({'success':True}), 200)
    else:
        return make_response("You must provide a data_id in the request args", 403)

if __name__ == '__main__':
	app.run(host=config.FLASK_HOST, port=config.FLASK_PORT)