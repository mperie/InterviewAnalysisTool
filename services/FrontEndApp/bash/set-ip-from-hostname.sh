#!/bin/sh
export CORPUS_DATA_DB_API_HOST="$(getent hosts $CORPUS_DATA_DB_API_HOST | awk '{ print $1 }')"
export CORPUS_INFO_DB_API_HOST="$(getent hosts $CORPUS_INFO_DB_API_HOST | awk '{ print $1 }')"
export IA_SERVER_HOST="$(getent hosts $AUTODISC_SERVER_HOST | awk '{ print $1 }')"