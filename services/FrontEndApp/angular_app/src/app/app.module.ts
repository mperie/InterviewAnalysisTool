import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { CorpusComponent } from './components/corpus/corpus.component';
import { CorpusCreationComponent } from './components/corpus/multi-collapser/corpus-creation/corpus-creation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import { UploadMultipleFilesComponent } from './components/utils/upload-multiple-files/upload-multiple-files.component';
import { FormsModule } from '@angular/forms';
import { MultiCollapserComponent } from './components/corpus/multi-collapser/multi-collapser.component';
import { CorpusCreationService } from './services/corpus-creation.service';
import { ListCorpusComponent } from './components/utils/list-corpus/list-corpus.component';
import {FilterPipe} from './pipes/filter.pipe';
import { TextManagerComponent } from './components/corpus/multi-collapser/text-manager/text-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CorpusComponent,
    CorpusCreationComponent,
    UploadMultipleFilesComponent,
    MultiCollapserComponent,
    ListCorpusComponent,
    FilterPipe,
    TextManagerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatIconModule,
    FormsModule
  ],
  providers: [CorpusCreationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
