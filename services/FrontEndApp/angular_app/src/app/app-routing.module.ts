import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CorpusComponent } from './components/corpus/corpus.component';
import { CorpusCreationComponent } from './components/corpus/multi-collapser/corpus-creation/corpus-creation.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'corpus-creation', component: CorpusCreationComponent},
  {path: 'corpus/:id', component: CorpusComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
