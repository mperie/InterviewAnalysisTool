import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CorpusCreationService } from './services/corpus-creation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Interview Analysis Tool';

  public path: string
  
  constructor(private router: Router, public corpusCreationService: CorpusCreationService) {
    this.path = ''
    router.events
    .subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.path = event.url
      }
    });
  }

  redirectToNewCorpus(){
    let p = this.corpusCreationService.createCorpus()
    p.then((corpusId) => {
      this.router.navigateByUrl("/corpus/"+corpusId)
    });
  }
}
