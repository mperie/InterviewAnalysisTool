export interface Corpus {
    ID: string;
    name: string;
    media_files : any;
    text_files: any;
    created_on: Date;
    description: string;
    [key: string]: any;
  }