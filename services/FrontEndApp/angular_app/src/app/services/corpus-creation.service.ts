import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Corpus } from '../entities/corpus';
import { CorpusDataDbService } from './REST-services/corpus-data-db.service';
import { IaServerService } from './REST-services/ia-server.service';

@Injectable({
  providedIn: 'root'
})
export class CorpusCreationService {

  currentCorpus?: Corpus;
  currentHost: string | undefined
  hosts: any
  currentCorpusListener: Subject<Corpus | undefined> = new BehaviorSubject<Corpus | undefined>(this.currentCorpus);



  constructor(private corpusDataDbService : CorpusDataDbService, private iaServerService: IaServerService) { }

  createCorpus() : Promise<any>{
    let p = new Promise((resolve) => {
    this.corpusDataDbService.createCorpusDataDbService().subscribe(
        (response) => {
          this.currentCorpus = response.data;
          resolve(this.currentCorpus?.ID)
        }
      );
    })
    return p;
  }

  getCorpusById(id:string) : Promise<any>{
    let p = new Promise((resolve) => {
        this.corpusDataDbService.getCorpusById(id).subscribe(
          response => {
            this.currentCorpus = response.data;
            this.currentCorpusListener.next(this.currentCorpus);
            resolve(this.currentCorpus?.ID)
          }
        )
    })
    return p;
  }

  update(updateDict: any): Promise<any>{
    let p = new Promise((resolve) => {
      if(this.currentCorpus){
        this.corpusDataDbService.update(this.currentCorpus.ID, updateDict).subscribe(
          response => {
            if(this.currentCorpus && response.data)
              console.log(response.data)
              // this.currentCorpus = response.data;
          }
        )
      }
    });
    return p;
  }

  getFiles(corpusId: string, file_type: string): Observable<any>{
    if(this.currentCorpus){
      let files = this.corpusDataDbService.getFiles(corpusId, file_type);
      files.subscribe(
        res => {
          if(this.currentCorpus){
            this.currentCorpus[file_type] = res.data
          }
          
        }
      );
      return files
    }else{
      return new Observable;
    }
  }

  getFile(corpusId: string | undefined, file_type: string | undefined, file_name: string | undefined): Observable<any>{
    if(this.currentCorpus){
      let file = this.corpusDataDbService.getFile(corpusId, file_type, file_name);
      file.subscribe(
        res => {
          if(this.currentCorpus){
           console.log(res)
          }
          
        }
      );
      return file
    }else{
      return new Observable;
    }
  }
  
  addFile(corpusId: string, file_type: string,formData: any){
    if(this.currentCorpus){
      let file = this.corpusDataDbService.addFile(corpusId, file_type, formData);
      file.subscribe(
        res => {
          if(this.currentCorpus){
           console.log(res)
          }
          
        }
      );
      return file
    }else{
      return new Observable;
    }
  }

  getHosts(): void {
    this.iaServerService.getHosts().subscribe(response => {
      if(response.success){
        this.hosts = response.data;
        this.currentHost = this.hosts[0];
      }
      // else{
      //   this.toasterService.showError(response.message ?? '', "Error getting hosts", {timeOut: 0, extendedTimeOut: 0});
      // }
    });
  }

  transcribeFiles(){
    if(this.currentCorpus && this.currentHost)
    this.iaServerService.transcribeMediaFiles(this.currentCorpus.ID, this.currentHost, this.currentCorpus);
  }

  
}
