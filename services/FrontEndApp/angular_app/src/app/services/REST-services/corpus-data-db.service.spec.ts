import { TestBed } from '@angular/core/testing';

import { CorpusDataDbService } from './corpus-data-db.service';

describe('CorpusDataDbService', () => {
  let service: CorpusDataDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorpusDataDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
