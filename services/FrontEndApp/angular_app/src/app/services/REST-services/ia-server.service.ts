import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

import { environment } from './../../../environments/environment';
import { RESTResponse, httpErrorResponseToRESTResponse, httpResponseToRESTResponse } from '../../entities/rest_response';
import { Observable, of } from 'rxjs';
import { Corpus } from '../../entities/corpus';
@Injectable({
  providedIn: 'root'
})
export class IaServerService {

  private iaServerUrl

  constructor(private http: HttpClient) {
    this.iaServerUrl = "http://" +"127.0.0.1:5000" // +":" + environment.IA_SERVER_PORT;
   }

   getHosts(): Observable<RESTResponse<string[]>> {
    return this.http.get<string[]>(
      this.iaServerUrl + "/hosts", { observe: 'response' })
      .pipe(
        map(response => { return httpResponseToRESTResponse<string[]>(response); }),
        catchError(response => { return of(httpErrorResponseToRESTResponse<string[]>(response)); })
      );
  }
  
  transcribeMediaFiles(corpusId: string, host: string, corpus: Corpus){
    let config ={
      task:{
          name: corpus.name,
          corpus_id: corpusId,
          description: corpus.description,
          type: "transcription",
          config: {
              host: host,
              preparing: {
                  fileFromDBToFolder: {
                    path: "tasks/tasks/transcription/media_files/",
                    type_file: "media_files"
                  }
              }
          }
      }
  }
    let res = this.http.post(`${this.iaServerUrl}/task`, config, {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'Accept': 'application/vnd.pgrst.object+json' // Get a single json element instead of an array 
      }),
      observe: 'response'
    })
    return res.subscribe(
      res => console.log("transcribe media start")
    )
  }
}
