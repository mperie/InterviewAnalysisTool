import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

import { environment } from './../../../environments/environment';
import { RESTResponse, httpErrorResponseToRESTResponse, httpResponseToRESTResponse } from '../../entities/rest_response';
import { Corpus } from 'src/app/entities/corpus';

import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CorpusDataDbService {

  private corpusDataDBUrl
  constructor(private http: HttpClient) { 
    // this.appDBUrl = "http://"+ environment.APPDB_HOST +":" + environment.APP_DB_API_PORT;
    this.corpusDataDBUrl = "http://"+ "127.0.0.1:5001" // +":" + environment.CORPUS_DATA_DB_API_PORT;
    // console.log(this.corpusDataDBUrl);
  }

  createCorpusDataDbService(){
    let d = this.formatDate(new Date())
    return this.http.post<Corpus>(
      this.corpusDataDBUrl + "/corpus",
      {
        "name": "",
        "media_files": {},
        "created_on": d,
        "description": ""
      },
      {observe: 'response'})
      .pipe(
        map(response => { return httpResponseToRESTResponse<Corpus>(response); }),
        catchError(response => { return of(httpErrorResponseToRESTResponse<Corpus>(response)); })
      );

  }

  getCorpusById(corpusId: string){
    return this.http.get<any>(
      `${this.corpusDataDBUrl}/corpus/${corpusId}`,
      {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        observe: 'response'
      }
    ).pipe(
      map(response => {
        if(response.body)
        response.body["ID"] = response.body["_id"];
        delete response.body["_id"];

        return httpResponseToRESTResponse<Corpus>(response); 
      }),
      catchError(response => { return of(httpErrorResponseToRESTResponse<Corpus>(response)); })
    );
  }

  getLightListCorpus(): Observable<RESTResponse<Corpus[]>>{
    return this.http.get<any[]>(
      this.corpusDataDBUrl+ "/corpus/light?filter={}",{
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        observe: 'response'
      }
    ).pipe(
      map(
        response => {
          response.body?.map(x =>{
              x["ID"] = x["_id"];
              delete x["_id"]})
          return httpResponseToRESTResponse<Corpus[]>(response); 
        }),
      catchError(response => { return of(httpErrorResponseToRESTResponse<Corpus[]>(response)); })
    );
  }

  addFile(corpusId: string, file_type: string,formData: any){
    // media_files
    return this.http.post(`${this.corpusDataDBUrl}/corpus/${corpusId}/${file_type}`, formData, {
      headers: new HttpHeaders({}),
      observe: 'response'
    })
  }

  getFiles(corpusId: string, file_type: string){
    return this.http.get<any>(`${this.corpusDataDBUrl}/corpus/${corpusId}/sub/${file_type}`,{
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response'
    }).pipe(
      map(response => {
        return httpResponseToRESTResponse<any>(response); 
      }),
      catchError(response => { return of(httpErrorResponseToRESTResponse<any>(response)); })
    );
  }

  getFile(corpusId: string | undefined, file_type: string | undefined, file_name: string | undefined){
    return this.http.get<any>(`${this.corpusDataDBUrl}/corpus/${corpusId}/${file_type}/${file_name}`,{
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'response'
    }).pipe(
      map(response => {
        return httpResponseToRESTResponse<any>(response); 
      }),
      catchError(
        response => { 
          return of(httpErrorResponseToRESTResponse<any>(response)); 
      })
    );
  }

  update(corpusID: string, updateDict: any){
    return this.http.patch<Corpus>(`${this.corpusDataDBUrl}/corpus/${corpusID}`, updateDict, {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'Accept': 'application/vnd.pgrst.object+json' // Get a single json element instead of an array 
      }),
      observe: 'response'
    }).pipe(
      map(response => { return httpResponseToRESTResponse<Corpus>(response); }),
      catchError(response => { return of(httpErrorResponseToRESTResponse<Corpus>(response)); })
    );
  }

  padTo2Digits(num: number) {
    return num.toString().padStart(2, '0');
  }

  formatDate(date: Date) {
    return (
      [
        date.getFullYear(),
        this.padTo2Digits(date.getMonth() + 1),
        this.padTo2Digits(date.getDate()),
      ].join('-') +
      ' ' +
      [
        this.padTo2Digits(date.getHours() -1),
        this.padTo2Digits(date.getMinutes()),
        this.padTo2Digits(date.getSeconds()),
      ].join(':')
    );
  }

  

}