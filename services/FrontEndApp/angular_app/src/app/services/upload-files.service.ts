import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CorpusDataDbService } from './REST-services/corpus-data-db.service';
import { CorpusCreationService } from './corpus-creation.service';
@Injectable({
  providedIn: 'root'
})
export class UploadFilesService {

  private baseUrl = 'http://localhost:5001';

  constructor(private http: HttpClient, private corpusCreationService : CorpusCreationService) { }

  upload(file: File, corpusId: string): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}/corpus/${corpusId}/media_files`, formData, {
      reportProgress: true,
      responseType: 'json',
    });

    return this.http.request(req);
  }

  getMediaFiles(corpusId:string): Observable<any> {
    return this.corpusCreationService.getFiles(corpusId, "media_files")
  }
}