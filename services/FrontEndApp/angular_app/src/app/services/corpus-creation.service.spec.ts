import { TestBed } from '@angular/core/testing';

import { CorpusCreationService } from './corpus-creation.service';

describe('CorpusCreationService', () => {
  let service: CorpusCreationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorpusCreationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
