import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list-corpus',
  templateUrl: './list-corpus.component.html',
  styleUrls: ['./list-corpus.component.scss']
})
export class ListCorpusComponent implements OnInit {

  constructor() { }

  @Input() list_corpus?: any;
  @Input() needRouter?: boolean;
  @Input() needChoice?: boolean;
  @Input() corpus?: any;
  @Output() corpusChange = new EventEmitter();
  @Output() triggerParentMethod = new EventEmitter<any>();
  @Input() searchText: string = "";


  ngOnInit(): void {
  }

  callParent(corpus : any){
    if(this.triggerParentMethod.observers.length >= 1){
      this.corpus = corpus;
      this.corpusChange.emit(this.corpus);
      this.triggerParentMethod.next(undefined);
    }
  }

}
