import { Component, OnInit, Input } from '@angular/core';
import { UploadFilesService } from 'src/app/services/upload-files.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Corpus } from 'src/app/entities/corpus';
import { CorpusCreationService } from 'src/app/services/corpus-creation.service';

@Component({
  selector: 'app-upload-multiple-files',
  templateUrl: './upload-multiple-files.component.html',
  styleUrls: ['./upload-multiple-files.component.scss']
})
export class UploadMultipleFilesComponent implements OnInit {

  @Input() corpus?: Corpus;
  selectedFiles: FileList | undefined;
  progressInfos :any = [];
  message = '';

  fileInfos: Observable<any> | undefined;

  constructor(private uploadService: UploadFilesService, private corpusCreationService: CorpusCreationService) { }
  
  ngOnInit(): void {
    if(this.corpusCreationService.currentCorpus){
      this.uploadService.getMediaFiles(this.corpusCreationService.currentCorpus.ID).subscribe(
        res => this.fileInfos = res.data
      );
    }
    
  }

  selectFiles(event: any) {
    this.progressInfos = [];
    this.selectedFiles = event.target.files;
  }

  uploadFiles() {
    this.message = '';
    if (this.selectedFiles != undefined){
      for (let i = 0; i < this.selectedFiles.length; i++) {
        this.upload(i, this.selectedFiles[i]);
      }
    }
    
  }

  upload(idx:number, file:any) {
    this.progressInfos[idx] = { value: 0, fileName: file.name };
    if (this.corpusCreationService.currentCorpus != undefined){
      this.uploadService.upload(file, this.corpusCreationService.currentCorpus.ID).subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress && event.total != undefined) {
            this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse && this.corpusCreationService.currentCorpus) {
            this.uploadService.getMediaFiles(this.corpusCreationService.currentCorpus.ID).subscribe(
              res => this.fileInfos = res.data
            );
          }
        },
        err => {
          this.progressInfos[idx].value = 0;
          this.message = 'Could not upload the file:' + file.name;
        });
    }
    
  }

}
