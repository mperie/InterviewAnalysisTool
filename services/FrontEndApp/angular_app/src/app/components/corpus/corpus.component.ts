import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CorpusCreationService } from 'src/app/services/corpus-creation.service';

@Component({
  selector: 'app-corpus',
  templateUrl: './corpus.component.html',
  styleUrls: ['./corpus.component.scss']
})
export class CorpusComponent implements OnInit {

  constructor(private route: ActivatedRoute, private corpusCreationService: CorpusCreationService) { }

  ngOnInit(): void{
    let corpusId = this.route.snapshot.paramMap.get('id');
    if(corpusId)
    this.corpusCreationService.getCorpusById(corpusId).then( (corpusId)=> {})
    this.corpusCreationService.getHosts()
    
  }

}
