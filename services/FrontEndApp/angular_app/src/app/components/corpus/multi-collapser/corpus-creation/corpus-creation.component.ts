import { Component, OnInit } from '@angular/core';
import { CorpusCreationService } from 'src/app/services/corpus-creation.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-corpus-creation',
  templateUrl: './corpus-creation.component.html',
  styleUrls: ['./corpus-creation.component.scss']
})
export class CorpusCreationComponent implements OnInit {

  objectKeys = Object.keys;

  style: any
  constructor(public corpusCreationService: CorpusCreationService) { }

  ngOnInit(): void {
    this.corpusCreationService.currentCorpusListener.subscribe({
      next: (theCorpus) => {
        if(theCorpus != undefined){
          setTimeout(()=>{
            let textareas = document.getElementsByName("descriptionArea")
            if(textareas)
            for(let i=0; i<textareas.length; i++){
              this.initialize((textareas[i]));
          }
          }
          )
          
        }
        
      }
    })
  }

  initialize(textarea:any){
    textarea.setAttribute("style", "min-width: 100%;height:" + (textarea.scrollHeight) + "px;overflow-y:hidden;");
    textarea.addEventListener("input", this.OnInput, false);
  }

  OnInput(event: any) {
    this.style.height = 0;
    this.style.height = (event.target.scrollHeight) + "px";
  }
}
