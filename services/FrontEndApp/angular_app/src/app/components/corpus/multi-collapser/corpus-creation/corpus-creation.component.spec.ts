import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpusCreationComponent } from './corpus-creation.component';

describe('CorpusCreationComponent', () => {
  let component: CorpusCreationComponent;
  let fixture: ComponentFixture<CorpusCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorpusCreationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CorpusCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
