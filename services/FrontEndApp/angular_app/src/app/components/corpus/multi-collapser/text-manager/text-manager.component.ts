import { Component, OnInit } from '@angular/core';
import { CorpusCreationService } from 'src/app/services/corpus-creation.service';

@Component({
  selector: 'app-text-manager',
  templateUrl: './text-manager.component.html',
  styleUrls: ['./text-manager.component.scss']
})
export class TextManagerComponent implements OnInit {

  constructor(public corpusCreationService: CorpusCreationService) { }
  
  style: any
  currentText: any
  file:any
  textFileName:string |undefined
  object_keys = Object.keys

  ngOnInit(): void {
    this.corpusCreationService.currentCorpusListener.subscribe({
      next: () => {this.selectText(undefined);}})
  }

  selectText(textName : string | undefined){
    if(this.corpusCreationService.currentCorpus != undefined && this.corpusCreationService.currentCorpus.text_files != undefined){
      let textTitle = this.chooseText(textName)
      this.file = this.corpusCreationService.getFile(
        this.corpusCreationService.currentCorpus?.ID,
        "text_files", 
        textTitle
      ).subscribe( res =>{
        this.currentText = res.data
        setTimeout(()=>{
          let textareas = document.getElementsByName("repliqueArea")
          if(textareas)
          for(let i=0; i<textareas.length; i++){
            this.initialize((textareas[i]));
          }
          this.disableButton(textTitle);
          this.textFileName = textTitle;
        }
        ) 
      })
    }
  }

  chooseText(textName : string | undefined){
    if (textName === undefined){
      return Object.keys(this.corpusCreationService.currentCorpus?.text_files)[0]
    }
    return textName;
  }

  initialize(textarea:any){
    textarea.setAttribute("this.style", "min-width: 100%;height:" + (textarea.scrollHeight) + "px;overflow-y:hidden;");
    textarea.addEventListener("input", this.OnInput, false);
    // textarea.addEventListener("change", this.OnInput, false);
    // textarea.addEventListener("merge", this.OnChange, false);
  }

  OnInput(event: any) {
    this.style.height = 0;
    this.style.height = (event.target.scrollHeight) + "px";
    this.style.overflow ="hidden";
  }

  OnChange(event: any) {
    this.style.height = 0;
    this.style.height = (event.detail) + "px";
    this.style.overflow ="hidden";
  }

  addToSpeaker(newSpeaker: string){
    if(newSpeaker != undefined && newSpeaker.trim().length > 0 && !this.currentText.speakers.includes(newSpeaker))
      this.currentText.speakers.push(newSpeaker)
      for (let i = 0; i < this.currentText.repliques.length; i++) {
        if (this.currentText.repliques[i].speaker === undefined) {
            this.currentText.repliques[i].speaker = newSpeaker;
        }
      }
  }

  removeSpeaker(speakerName :string){
    const index = this.currentText.speakers.indexOf(speakerName);
    if (index !== -1) {
      this.currentText.speakers.splice(index, 1);
    }
    for (let i = 0; i < this.currentText.repliques.length; i++) {
      if (this.currentText.repliques[i].speaker === speakerName) {
          this.currentText.repliques[i].speaker = this.currentText.speakers[0];
      }
    }
  }

  replaceSpeaker(speakerOrigin: string, newSpeaker: string){
    for (let i = 0; i < this.currentText.speakers.length; i++) {
      if (this.currentText.speakers[i] === speakerOrigin) {
          this.currentText.speakers[i] = newSpeaker;
      }
    }
    for (let i = 0; i < this.currentText.repliques.length; i++) {
      if (this.currentText.repliques[i].speaker === speakerOrigin) {
          this.currentText.repliques[i].speaker = newSpeaker;
      }
    }
  }

  merge(repliqueIndex: any, below: boolean){
    let idx = -1;
    if(below)
      idx = 1
    if(repliqueIndex + idx>= 0 && repliqueIndex + idx< this.currentText.repliques.length){
      if(below){
        this.currentText.repliques[repliqueIndex].text += this.currentText.repliques[repliqueIndex+idx].text
      }else{
        this.currentText.repliques[repliqueIndex].text = this.currentText.repliques[repliqueIndex+idx].text + this.currentText.repliques[repliqueIndex].text
      }
      this.currentText.repliques.splice(repliqueIndex+idx, 1);
      
      // update cell size after merge 
      let textareas = document.getElementsByName("repliqueArea");
      if(textareas){
        setTimeout( ()=>{
          if(idx > 0){
            textareas[repliqueIndex].dispatchEvent(new CustomEvent('input'));
          }else{
            textareas[repliqueIndex+idx].dispatchEvent(new CustomEvent('input'));
          }
          
        })           
      }
      
      
    }
  }

  removeReplique(repliqueIndex: any){
    this.currentText.repliques.splice(repliqueIndex, 1);
  }

  addReplique(repliqueIndex: any){
    let newReplique = {
      "start": undefined,
      "end": undefined,
      "speaker": this.currentText.speakers[0],
      "text": ""
    }
    this.currentText.repliques.splice(repliqueIndex+1, 0, newReplique);
  }

  disableButton(name : string){
    let button = document.getElementsByName("selectTextFile");
    let idx = Object.keys(this.corpusCreationService.currentCorpus?.text_files).indexOf(name)
    for(let i = 0; i < button.length; i++){
      if(i != idx){
        (button[i]as any).disabled = false;
      }else{
        (button[i] as any).disabled = true;
      } 
    }
  }

  saveCurrentFile(){
    if(this.corpusCreationService.currentCorpus && this.textFileName != undefined){
      let myFile = new File([JSON.stringify(this.currentText)], this.textFileName)
      let formData: FormData = new FormData();
      formData.append('file', myFile);
      this.corpusCreationService.addFile(this.corpusCreationService.currentCorpus?.ID, "text_files", formData)
    }
    
  }

  downloadCurrentFile(){
    var text = this.convertJsonRepliqueToText();
    var element = document.createElement('a');
    element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(text));
    let fileName = ""
    if(this.textFileName){
      fileName = this.textFileName.replace(" ", ".");
      fileName = fileName.replace(".json", ".txt");
    }
    
    element.setAttribute('download', fileName);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  convertJsonRepliqueToText(){
    let currentTxt = "";
    for(let i=0; i < this.currentText.repliques.length; i++){
      currentTxt += this.currentText.repliques[i].speaker
      currentTxt += ":\n"
      currentTxt += this.currentText.repliques[i].text
      currentTxt += "\n\n"
    }
    return currentTxt
  }

  test(){
    console.log("oui")
  }

}
