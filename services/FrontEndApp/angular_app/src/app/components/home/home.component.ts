import { Component, OnInit } from '@angular/core';
import { Corpus } from 'src/app/entities/corpus';
import { CorpusDataDbService } from 'src/app/services/REST-services/corpus-data-db.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  list_corpus: Corpus[] = [];
  sortByDateAsc: boolean = true; 
  searchText = '';

  constructor(private corpusDataDbService: CorpusDataDbService) { }

  ngOnInit() {
    this.getListCorpus();
  }

  getListCorpus(): void {
    this.corpusDataDbService.getLightListCorpus()
    .subscribe(response => {
      if(response.success){
        this.sortByDateAsc = true; 
        this.list_corpus = response.data ?? [];
        this.sortListCorpusByDate();
      }
      else {
        console.log(response.message ?? '', "Error listing corpus")
        // this.toasterService.showError(response.message ?? '', "Error listing list_corpus");
      }
    });
  }

  sortListCorpusByDate(): void {
    this.sortByDateAsc = !this.sortByDateAsc;
    this.list_corpus.sort((a, b) => {
      return this.sortByDateAsc ? +new Date(a.created_on) - +new Date(b.created_on) : +new Date(b.created_on) - +new Date(a.created_on);
    });
  }
}
