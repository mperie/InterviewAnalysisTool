export const environment = {
  production: true,
  CORPUS_DATA_DB_API_PORT: (window as any)["env"]["CORPUS_DATA_DB_API_PORT"],
  CORPUS_DATA_DB_API_HOST: (window as any)["env"]["CORPUS_DATA_DB_API_HOST"],
  CORPUS_INFO_DB_API_PORT: (window as any)["env"]["CORPUS_INFO_DB_API_PORT"],
  CORPUS_INFO_DB_API_HOST: (window as any)["env"]["CORPUS_INFO_DB_API_HOST"],
  IA_SERVER_HOST: (window as any)["env"]["IA_SERVER_HOST"],
  IA_SERVER_PORT: (window as any)["env"]["IA_SERVER_PORT"],
  
  
  // AUTODISC_SERVER_PORT: (window as any)["env"]["AUTODISC_SERVER_PORT"],
  // AUTODISC_SERVER_HOST: (window as any)["env"]["AUTODISC_SERVER_HOST"],
};
