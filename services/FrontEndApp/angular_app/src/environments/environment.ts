// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  CORPUS_DATA_DB_API_PORT: (window as any)["env"]["CORPUS_DATA_DB_API_PORT"],
  CORPUS_DATA_DB_API_HOST: (window as any)["env"]["CORPUS_DATA_DB_API_HOST"],
  CORPUS_INFO_DB_API_PORT: (window as any)["env"]["CORPUS_INFO_DB_API_PORT"],
  CORPUS_INFO_DB_API_HOST: (window as any)["env"]["CORPUS_INFO_DB_API_HOST"],
  IA_SERVER_PORT: (window as any)["env"]["IA_SERVER_PORT"],
  IA_SERVER_HOST:(window as any)["env"]["IA_SERVER_HOST"],
  debug: (window as any)["env"]["debug"] || false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
