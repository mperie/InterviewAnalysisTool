(function(window) {
    window.env = window.env || {};
  
    // Environment variables
    window["env"]["CORPUS_DATA_DB_API_PORT"] = "5001";
    window["env"]["CORPUS_DATA_DB_API_HOST"] = "127.0.0.1";
    window["env"]["CORPUS_INFO_DB_API_PORT"] = "3000";
    window["env"]["CORPUS_INFO_DB_API_HOST"] = "127.0.0.1";
    window["env"]["IA_SERVER_PORT"] = "5000"
    window["env"]["IA_SERVER_HOST"] = "127.0.0.1"
    window["env"]["debug"] = "";
  })(this);