(function(window) {
    window.env = window.env || {};
  
    // Environment variables
    window["env"]["CORPUS_DATA_DB_API_PORT"] = "${CORPUS_DATA_DB_API_PORT}";
    window["env"]["CORPUS_DATA_DB_API_HOST"] = "${CORPUS_DATA_DB_API_HOST}";
    window["env"]["CORPUS_INFO_DB_API_PORT"] = "${CORPUS_INFO_DB_API_PORT}";
    window["env"]["CORPUS_INFO_DB_API_HOST"] = "${CORPUS_INFO_DB_API_HOST}";
    window["env"]["IA_SERVER_PORT"] = "${IA_SERVER_PORT}"
    window["env"]["IA_SERVER_HOST"] = "${IA_SERVER_HOST}"
    window["env"]["debug"] = "${DEBUG}";
  })(this);