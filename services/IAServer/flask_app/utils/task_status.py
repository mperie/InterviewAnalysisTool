from enum import IntEnum

class TaskStatusEnum(IntEnum):
    DONE = 0
    RUNNING = 1
    CANCELLED = 2
    ERROR = 3
    PREPARING = 4