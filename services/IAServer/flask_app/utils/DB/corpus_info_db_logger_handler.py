from logging import StreamHandler
import logging
import requests
import re

class CorpusInfoDBLoggerHandler(StreamHandler):
    
    def __init__(self, base_url, task_id):
        StreamHandler.__init__(self)
        self.log_levels_id ={
            'NOTSET' : 1,
            'DEBUG' : 2,
            'INFO' : 3,
            'WARNING' : 4,
            'ERROR' : 5,
            'CRITICAL' : 6
        }
        self.task_id = task_id
        self.base_url = base_url # http://127.0.0.1:3000
        # self.setLevel(logging.NOTSET)
        # self.setFormatter(logging.Formatter('%(name)s - %(levelname)s - %(message)s'))


    def emit(self, record: logging.LogRecord):
        if self.task_id == record.args["task_id"]:
            if(len(re.sub("[^0-9]", "", record.levelname)) >0):
                levelname = int(re.sub("[^0-9]", "", record.levelname))
            else:
                levelname = self.log_levels_id[record.levelname]
            checkpoint_id = self.get_checkpoint_id_from_seed_fn(record.args["seed"])
            self.save(checkpoint_id, record.args["seed"], levelname, record.args["id"], record.msg)
            
    
    def save(self, log_id: str, message: str):
        response = requests.post(self.base_url+"/logs", 
            json={
                    "task_id":self.task_id,
                    "name":log_id,
                    "error_message": message
                }
        )
        print(response)
