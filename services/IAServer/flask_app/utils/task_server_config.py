import os
from os import environ as env
from typing import get_type_hints, Union


def _parse_bool(val: Union[str, bool]) -> bool:  # pylint: disable=E1136 
    return val if type(val) == bool else val.lower() in ['true', 'yes', '1']

class TaskServerConfigError(Exception):
    pass

class TaskServerConfig():
    FLASK_HOST : str = '0.0.0.0'
    FLASK_PORT: int = 5000
    SSH_CONFIG_FILE : str = None
    CORPUS_DATA_DB_CALLER_HOST : str = "corpus-data-db-api" # "127.0.0.1"
    CORPUS_DATA_DB_CALLER_PORT : str = "80" # "5001"
    CORPUS_INFO_CALLER_HOST : str = "corpus-info-db-api" #"127.0.0.1"
    CORPUS_INFO_CALLER_PORT : str = "80" # "3000"

    def __init__(self) -> None:
        print("Entering constructor")
        print(f"Annotations: {self.__annotations__}")
        for field in self.__annotations__:
            default_value = getattr(self, field, None)
            print(f"Default value for field {field} is {default_value}")
            if default_value is None and env.get(field) is None:
                raise TaskServerConfigError('The {} field is required'.format(field))

            var_type = get_type_hints(TaskServerConfig)[field]

            try: 
                if var_type == bool:
                    value = _parse_bool(env.get(field, default_value))
                else:
                    value = var_type(env.get(field, default_value))

                print(f"Setting new value for field {field} with {value}")
                self.__setattr__(field, value)
            except:
                raise TaskServerConfigError('Unable to cast value of "{}" to type "{}" for "{}" field'.format(
                    env[field],
                    var_type,
                    field
                    )
                )