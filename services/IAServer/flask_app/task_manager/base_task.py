from utils import TaskServerConfig
from utils.DB import CorpusDataDBCaller, CorpusInfoDBCaller



import os
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

class BaseTask():

    def __init__(self, id, task_config) -> None:
        self.id = id
        self.task_config = task_config

        self.taskServerConfig = TaskServerConfig()
        self._corpus_data_db_caller = CorpusDataDBCaller('http://{}:{}'.format(self.taskServerConfig.CORPUS_DATA_DB_CALLER_HOST, self.taskServerConfig.CORPUS_DATA_DB_CALLER_PORT))
        self._corpus_info_db_caller = CorpusInfoDBCaller("http://{}:{}".format(self.taskServerConfig.CORPUS_INFO_CALLER_HOST, self.taskServerConfig.CORPUS_INFO_CALLER_PORT))

#region public launching
    def prepare(self):
        raise NotImplementedError()
    
    def start(self):
        raise NotImplementedError()

    def stop(self):
        raise NotImplementedError()
    
    def reload(self):
        raise NotImplementedError()
#endregion

#region callbacks

    def on_save_data(self, path:str, file_type: str, **kwargs):
        """
        save data in db
        Args:
            paths: list[(disk_path, file_type_in_db)]
        """
        files_to_save = {}
        files_to_save["name"] = open(path, "rb")
        self._corpus_data_db_caller("/corpus/"+self.task_config["task"]["corpus_id"]+"/"+file_type, files=files_to_save)

    def clean_db(self, file_type: str, file_name: str, **kwargs):
        self._corpus_data_db_caller("/corpus"+self.task_config["task"]["corpus_id"]+"/"+file_type)

    def on_finished(self, **kwargs):
        pass

    def on_error(self, **kwargs):
        pass

#endregion

#region utils
    def get_media_files(self):
        route = "/corpus/{}/sub/{}".format(self.id, "media_files")
        media_files = self._corpus_data_db_caller(self.id, route)
        for media_file in media_files:
            route = "/corpus/{}/{}/{}".format(self.id, "media_files", media_file)
            file = self._corpus_data_db_caller(self.id, route)
            print(file)
#end region

#region preparing
    def fileFromDBToFolder(self, **kwargs):
        print("upload media")
        # get media files
        route = "/corpus/{}/sub/{}".format(self.task_config["task"]["corpus_id"], kwargs["type_file"])
        mediaFileList = self._corpus_data_db_caller.get(route)
        for mediaFileName in mediaFileList:
            route = "/corpus/{}/media_files/{}".format(self.task_config["task"]["corpus_id"], mediaFileName)
            mediaFile = self._corpus_data_db_caller.get_file(route)
            # print(mediaFile)
            raw = mediaFile.content

            # Write to file
            filename = mediaFile.headers.get("Content-Disposition").split("filename=")[1]
            path = __location__ + "/../../../../" + kwargs["path"] + str(self.id)
            if not os.path.exists(path):
                os.makedirs(path)
            with open(path+"/"+filename, 'wb') as f:
                f.write(raw)

#endregion