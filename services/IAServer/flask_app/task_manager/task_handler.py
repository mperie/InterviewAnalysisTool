from utils import TaskServerConfig, TaskStatusEnum
from utils.DB import CorpusInfoDBCaller, CorpusInfoDBMethods
from task_manager import LocalTask, RemoteTask

import datetime
import traceback
import threading

class TaskHandler():
    def __init__(self) -> None:
        self._tasks = []
        self._taskServerConfig = TaskServerConfig()
        self._corpus_info_db_caller = CorpusInfoDBCaller("http://{}:{}".format(self._taskServerConfig.CORPUS_INFO_CALLER_HOST, self._taskServerConfig.CORPUS_INFO_CALLER_PORT))

    def add_task(self, parameters):
        try:
            # Add task in DB and obtain the id
            exp_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M %Z")
            response = self._corpus_info_db_caller("/tasks", CorpusInfoDBMethods.POST, {
                                        "name": parameters['task']['name'],
                                        "corpus_id": parameters['task']['corpus_id'],
                                        "description": parameters['task']['description'],
                                        "type": parameters['task']['type'],
                                        "created_on": exp_date,
                                        "config": parameters['task']['config'],
                                        "progress": 0,
                                        "task_status": int(TaskStatusEnum.PREPARING),
                                        "archived": False,
                                     })
            id = response.headers["Location"].split(".")
            id = int(id[1])

            task = self._create_task(id, parameters)

            prepare_and_start_task_async = threading.Thread(target=self.prepare_and_start_task_async, args=(task,))
            prepare_and_start_task_async.start()

            return id
        except Exception as err:
            message = "Error when creating task: {}".format(traceback.format_exc())
            try:
                # self.remove_task(id, task_status=TaskStatusEnum.ERROR)
                pass
            except Exception as second_err:
                message += "\nError when removing task: {}".format(traceback.format_exc())
            finally:
                # self._corpus_info_db_caller("/preparing_logs", 
                #                 CorpusInfoDBMethods.POST, {
                #                     "task_id":id,
                #                     "message": message
                #                 }
                #             )
                raise Exception(message)

    def _create_task(self, id, parameters):
        # Create task
        if parameters["task"]["config"]["host"] == "local":
            task = LocalTask(id, parameters)
        else:
            task = RemoteTask(id, parameters)
        
        # Add it in the list 
        self._tasks.append(task)

        return task

    def prepare_and_start_task_async(self, task):
        # Prepare for start
        task.prepare()

        # Start the task
        task.start()    