from task_manager import BaseTask
from tasks import create, start

import threading
# from Tasks
class LocalTask(BaseTask):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pipelines = []
        self._running_tasks = []

    def prepare(self):
        for preparing_method in self.task_config["task"]["config"]["preparing"]:
            method = getattr(self, preparing_method)
            result = method(**self.task_config["task"]["config"]["preparing"][preparing_method])
        callbacks = {
            "save": self.on_save_data,
            "finished": self.on_finished,
            "error": self.on_error
        }
        self._pipelines.append(create(self.task_config, self.id, callbacks))

    
    def start(self):
        # print("Starting local experiment with id {} and {} seeds".format(self.id, self.experiment_config['experiment']['config']['nb_seeds']))
        # self._running_tasks = []
        # response = self._app_db_caller("/experiments?id=eq.{}".format(self.id), 
        #                         AppDBMethods.PATCH, 
        #                         {"exp_status": ExperimentStatusEnum.RUNNING})
        # self._app_db_caller("/preparing_logs", 
        #                         AppDBMethods.POST, {
        #                             "experiment_id":self.id,
        #                             "message": "the experiment start"
        #                         }
        #                     )
        for pipeline in self._pipelines:
            task = threading.Thread(target=start, args=(pipeline,))
            task.start()
            self._running_tasks.append(task)