from copy import copy
import os
import json
from time import sleep
from pexpect import pxssh
import threading

from utils.task_status import TaskStatusEnum
from utils.DB import CorpusInfoDBLoggerHandler, CorpusDataDBCaller, CorpusInfoDBMethods, CorpusInfoDBCaller
from task_manager import BaseTask
from utils import list_profiles, parse_profile

class RemoteTask(BaseTask):
    
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.__host_profile = parse_profile(next(profile[1] for profile in list_profiles() if profile[0] == self.task_config["task"]["config"]["host"]))
        ### create connection
        self.shell = pxssh.pxssh()
        self.ssh_config_file_path = self.taskServerConfig.SSH_CONFIG_FILE
        self.shell.login(self.__host_profile["ssh_configuration"], ssh_config=self.ssh_config_file_path)
        self.task_finished = False
        self.corpus_info_db_logger_handler = CorpusInfoDBLoggerHandler('http://{}:{}'.format(self.taskServerConfig.CORPUS_INFO_CALLER_HOST, self.taskServerConfig.CORPUS_INFO_CALLER_PORT), self.id)

    def __close_ssh(self):
        if "close_command" in self.__host_profile:
            self.shell.sendline(self.__host_profile["close_command"])
            self.shell.sendline("echo 'close command end'")
            while not "close command end" in self.shell.after.decode():
                self.shell.expect("close command end")
        try:
            self._monitor_async.join()
        except:
            pass
        self.shell.close()
        if self.killer_shell is not None:
            self.killer_shell.close()
        if "post_command" in self.__host_profile:
            os.system(self.__host_profile["post_command"])
            
    
    def prepare(self):
        print("Preparing...")
        for preparing_method in self.task_config["task"]["config"]["preparing"]:
            method = getattr(self, preparing_method)
            result = method(**self.task_config["task"]["config"]["preparing"][preparing_method])
        if "prepare_command" in self.__host_profile:
            # self.shell.sendline("su -")
            self.shell.sendline(self.__host_profile["prepare_command"])
        self.send_packaged_task_to_remote_server()

    def start(self):
        # move to work directory
        self.shell.sendline('cd {}'.format(self.__host_profile["work_path"]))
        # make command we will execute to launch the task
        exec_command = self.__host_profile["execution"]
        exec_command = exec_command.replace(
            "$ARGS",
             "--config_file {}/parameters_remote.json --task_id {}"
             .format(
                 self.__host_profile["work_path"], 
                 self.id
             )
        )
        exec_command = exec_command.replace("$TASK_ID", self.__host_profile["work_path"]+"/run_ids/"+str(self.id))

        # execute command
        print("Sending exec command...")
        self.shell.sendline(exec_command)
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "the command to run the task on the remote server has been launched"
                                }
                            )
        # get run id of each python who have been launched
        self.__run_id = self.__get_run_id() #TODO save run_id in db
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "the command has been taken into account by the server"
                                }
                            )
        print("Fetched run_id")
        # read log file to manege remote task
        self._monitor_async = threading.Thread(target=self._monitor)
        self._monitor_async.start()

    def reload(self):
        pass

    def stop(self):
        ### create new shell who will kills process launched by first shell
        ## open ssh connection
        self.killer_shell = pxssh.pxssh()
        self.killer_shell.login(self.__host_profile["ssh_configuration"], ssh_config=self.ssh_config_file_path)
        ## create and exec cancellation command
        cancellation_command = self.__host_profile["cancellation"].replace("$RUN_ID", self.__run_id)
        self.killer_shell.sendline(cancellation_command)
        self.killer_shell.sendline("for pid in $(pgrep tail); do kill -9 $pid; done;")
        ## declare all seed finished(all cancelled)
        # self.threading_lock.acquire()
        # self.nb_seeds_finished = self.task_config["experiment"]["config"]["nb_seeds"]
        # self.threading_lock.release()
        ## close properly ssh connection
        self.__close_ssh()

    def send_packaged_task_to_remote_server(self):
        to_push_folder_path = self.__host_profile["local_tmp_path"]+"/remote_task/push_to_server" # where we saved remote files need to be pushed on server
        if(not os.path.exists(to_push_folder_path)):
            os.makedirs(to_push_folder_path)
        ssh_log_folder = self.__host_profile["local_tmp_path"]+"/remote_task/ssh_log" # where we saved remote ssh log
        if(not os.path.exists(ssh_log_folder)):
            os.makedirs(ssh_log_folder)

        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "send necessary files to the remote server"
                                }
                            )
        ## push task
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "package and send the task to the remote server"
                                }
                            )
        # make path
        task_path =os.path.dirname(os.path.realpath(__file__))+"/../../../../tasks/tasks/{}".format(self.task_config["task"]["type"]) 
        task_path_tar = to_push_folder_path+"/tasks.tar.gz"
        # push and untar task
        self.__tar_local_folder(task_path, task_path_tar)
        self.__push_folder(task_path_tar, self.__host_profile["work_path"])
        self.__untar_folder(self.__host_profile["work_path"]+"/tasks.tar.gz", self.__host_profile["work_path"])
        
        ## push callbacks
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "package and send the callbacks to the remote server"
                                }
                            )
        # make path
        callbacks_path =os.path.dirname(os.path.realpath(__file__))+"/../../../../tasks/tasks/{}".format("callbacks") 
        callbacks_path_tar = to_push_folder_path+"/callbacks.tar.gz"
        # push and untar task
        self.__tar_local_folder(callbacks_path, callbacks_path_tar)
        self.__push_folder(callbacks_path_tar, self.__host_profile["work_path"])
        self.__untar_folder(self.__host_profile["work_path"]+"/callbacks.tar.gz", self.__host_profile["work_path"])
        ## push slurm file
        # make path
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "send config files to the remote server"
                                }
                            )
        additional_file_path = os.path.dirname(os.path.realpath(__file__)) + "/../../../../configs/remote_tasks/additional_files"
        additional_file_path_tar = to_push_folder_path+"/additional_files.tar.gz"
        self.__tar_local_folder(additional_file_path, additional_file_path_tar)
        self.__push_folder(additional_file_path_tar, self.__host_profile["work_path"])
        self.__untar_folder(self.__host_profile["work_path"]+"/additional_files.tar.gz", self.__host_profile["work_path"])

        ## push parameters file (json)
        # save json on disk
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "send parameters files to the remote server"
                                }
                            )
        json_file_path = to_push_folder_path + "/parameters_remote.json"
        with open(json_file_path, 'w+') as fp:
            json.dump(self.task_config, fp) # TODO cleared config probably doesn't exist
        # push json on remote server
        self.__push_folder(json_file_path, self.__host_profile["work_path"])

        # make logs folder on remote server
        self.shell.sendline("mkdir {}/{}".format(self.__host_profile["work_path"], "logs"))
        self.shell.sendline("mkdir {}/{}".format(self.__host_profile["work_path"], "run_ids"))
        self.shell.sendline("mkdir {}/{}".format(self.__host_profile["work_path"], "out"))
        self.shell.sendline("mkdir {}/{}".format(self.__host_profile["work_path"], "logs"))
        self.shell.sendline("mkdir {}/{}".format(self.__host_profile["work_path"], "tasks"))

        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "All necessary files have been sent to the remote server and are ready to be used"
                                }
                            )

    def __tar_local_folder(self, folder_src, tar_path):
        folder_src_split = folder_src.split("/")
        target_folder = folder_src_split[-1] # get last child folder name
        # remove last child folder from path
        index = folder_src.rfind(target_folder)
        parent_folder = folder_src[:index]
        excluder = "" # , "*.pickle" , "*/media_files/*"
        for toExclude in ["*/audio/*", "*/text/*", "*.pickle"]:
            excluder += "--exclude={} ".format(toExclude)
        os.system('cd {} && tar {} -czvf {} {}'.format(parent_folder, excluder, tar_path, target_folder))

    def __push_folder(self, folder_path_local, folder_path_remote):
        os.system('scp -r {} {}:{}'.format(folder_path_local, self.__host_profile["ssh_configuration"], folder_path_remote))
    
    def __pull_folder(self, folder_path_remote, folder_path_local):
        os.system('scp -r {}:{} {}'.format(self.__host_profile["ssh_configuration"], folder_path_remote, folder_path_local))

    def __untar_folder(self, tar_path, folder_path_dest):
        self.shell.sendline('tar -xvf {} -C {} \n'.format(tar_path, folder_path_dest))

    def __pull_file(self, remote_path, local_folder, file_name):
        """
        brief: pull all file create by a callback
        param: log : list
               remote_path: string : path
               sub_folders: string list : folder's name
               run_idx: string : run_idx
        """
        remote_path = remote_path.replace(file_name, "")
        auto_disc_parent_folder = remote_path.find(remote_path.split('/')[-5])
        local_folder += remote_path[auto_disc_parent_folder-1:]
        local_path = "/".join(local_folder.split("/")[:-2])
        if not os.path.exists(local_folder):
            os.makedirs(local_folder)
        self.__pull_folder(remote_path, local_path)
        return local_folder

    def __get_run_id(self):
        self.shell_to_get_run_id = pxssh.pxssh()
        self.shell_to_get_run_id.login(self.__host_profile["ssh_configuration"], ssh_config=self.ssh_config_file_path)

        while not self.test_file_exist(self.__host_profile["work_path"]+"/run_ids/{}".format(self.id)):
            self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "waiting for the server"
                                }
                            )
            sleep(self.__host_profile["check_task_launched_every"])

        while not "[RUN_ID_start]" in self.shell_to_get_run_id.before.decode():
            self.shell_to_get_run_id.prompt()
            self.shell_to_get_run_id.sendline('cat '+self.__host_profile["work_path"]+"/run_ids/{}".format(self.id))

        lines = self.shell_to_get_run_id.before.decode().split("\n")
        self.shell_to_get_run_id.close()
        
        for line in lines:
            if line.startswith("[RUN_ID_start]") and "[RUN_ID_stop]" in line:
                line = line.replace("RUN_ID_stop", "")
                line = line.replace("RUN_ID_start", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.strip()
                response = self._corpus_info_db_caller("/tasks?id=eq.{}".format(self.id), 
                                CorpusInfoDBMethods.PATCH, 
                                {"remote_run_id": line})
                return line
    
    def test_file_exist(self, file_path):
        self.shell.prompt(timeout=2)
        self.shell.sendline('test -f '+file_path + '&& echo "File exist" || echo "File does not exist"')
        self.shell.sendline('echo "test_file_end"')
        self.shell.expect('test_file_end')
        lines = self.shell.before.decode().split("\n")
        for line in lines:
            if "File exist" == line.strip():
                return True
            if "File does not exist" == line.strip():
                return False
        return False

    def _monitor(self):
        print("Starting monitoring")
        ### monitor remote task
        ## make path
        local_folder = self.__host_profile["local_tmp_path"]+"/remote_task/out" # where we saved remote tasks output before puting them in our db
        if(not os.path.exists(local_folder)):
            os.makedirs(local_folder)
        ## listen log file and do the appropriate action
        while not self.test_file_exist(self.__host_profile["work_path"]+"/logs/{}.log".format(self.id)):
            self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "waiting for the server"
                                }
                            )
            sleep(self.__host_profile["check_task_launched_every"])
        self.shell.sendline(
            'tail -F -n +1 {}'
            .format(self.__host_profile["work_path"]+"/logs/{}.log".format(self.id))
        )
        #change task status from preparing to running in db 
        response = self._corpus_info_db_caller("/tasks?id=eq.{}".format(self.id), 
                                CorpusInfoDBMethods.PATCH, 
                                {"tasks_status": TaskStatusEnum.RUNNING})
        self._corpus_info_db_caller("/preparing_logs", 
                                CorpusInfoDBMethods.POST, {
                                    "task_id":self.id,
                                    "message": "the task start"
                                }
                            )
        self.__listen_log_file(local_folder)

    def __listen_log_file(self, local_folder):
        """
        Brief: As long as the seeds have not finished read log file and do appropriate action
        """
        try:
            current_log_line = previous_log = previous_log_id = None
            previous_message = None
            message = None
            while not self.task_finished:
                self.shell.expect('\n', timeout=360)
                current_log_line = self.shell.before.decode()
                if "IAnalyser -" in current_log_line:
                    ## determine action to do
                    logger_name, log_type, task_id, log_id, message = self.__parse_log(copy(current_log_line), local_folder)
                    
                    if self.__log_not_in_db(log_id):
                        if previous_log is not None and previous_message is not None:
                            print(previous_message)
                            self.corpus_info_db_logger_handler.save(
                                previous_log_id,
                                previous_message)
                            
                        previous_log = current_log_line
                        previous_log_id = log_id
                        previous_message = message
                else:
                    #aggregates the text to the next log we will send to DB
                    if previous_log is not None and previous_message is not None:
                        previous_message += current_log_line
                    elif message is not None:
                        previous_log = self.shell.before.decode()
                        previous_message = message
            if "[ERROR]" in previous_message:
                #In this case we leave while with only a part of the message
                self.shell.prompt()
                error_message = self.shell.before.decode()
                #get all text and keep only the error
                previous_message = error_message[error_message.find(previous_message):]
            #send last log
            print(previous_message)
            self.corpus_info_db_logger_handler.save(
                log_id,
                previous_message)
            ## close properly ssh connection
            self.stop()
        except Exception as ex:
            print("unexpected error occurred. checked the logs of your remote server")
            print(ex)
            # self.callback_to_all_running_seeds(lambda seed, current_checkpoint_id : super().on_error(seed, current_checkpoint_id))
    
    def __parse_log(self, log, local_folder):
        """
        Brief: read current log line and choose the appropriate action
        param: log : string
        local_folder : string
        """
        logger_name, log_type, task_id, log_id, message = self.__get_log_part(log)
        # logger_name, log_level_name, seed_number, log_id, message = self.__get_log_part(log)
        if not self.__log_not_in_db(log_id):
            return logger_name, log_type, task_id, log_id, message

        if log_type == "NEW FILE":
            output_type, remote_path, sub_folders, file_name = self.__get_saved_files_to_pull(message)
            # super().on_progress(seed_number, run_idx + 1)
            if sub_folders is not None:
                local_path = self.__pull_file(remote_path, local_folder, file_name)
                self.on_save_data("{}{}".format(local_path, file_name), output_type)
        elif log_type == "TASK FINISHED":
            super().on_finished()
            self.task_finished = True
        # elif self.__is_discovery(message):
        #     super().on_progress(seed_number)
        # elif self.__is_saved(message):
        #     super().on_save(seed_number, self._get_current_checkpoint_id(seed_number))     
        # elif self.__is_error(message):
        #     super().on_error(seed_number, self._get_current_checkpoint_id(seed_number))
        #     is_current_seed_finished = True
        
        return logger_name, log_type, task_id, log_id, message
    
    def __get_saved_files_to_pull(self, log):
        splitted_log = log.split('-')
        del splitted_log[0]
        output_type = splitted_log[0].replace("OUTPUT_TYPE", "").strip() 
        remote_path = splitted_log[1].replace("FILE_PATH", "").strip()
        sub_folders, file_name = "/".join(remote_path.split('/')[-5:-1]), remote_path.split('/')[-1]
        return output_type, remote_path, sub_folders, file_name

    def __get_log_part(self, log):
        log_splitted = log.split("-")

        logger_name = log_splitted[0].strip()
        log_type = log_splitted[1].strip()
        task_id = log_splitted[2].replace("TASK_ID", "").strip()
        log_id = log_splitted[3].replace("LOG_ID", "").strip()
        message = ""
        for i in range(4, len(log_splitted)):
            message += "-" + log_splitted[i]
        return logger_name, log_type, task_id, log_id, message
    
    def __log_not_in_db(self, log_name):
        response = self._corpus_info_db_caller("/logs?&name=eq.{}".format(log_name), CorpusInfoDBMethods.GET, None)
        return response.content.decode() == '[]'