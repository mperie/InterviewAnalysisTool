#### Add auto_disc lib to path ####
import sys
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(dir_path, "../../../"))
#### Add auto_disc lib to path ####

from flask import Flask, request, jsonify, make_response
from flask_cors import CORS
from task_manager import TaskHandler
from utils import list_profiles



app = Flask(__name__)
CORS(app)
task = TaskHandler()

@app.route('/task', methods=["post"])
def make_task():
    id = task.add_task(request.json)
    return make_response(jsonify({
        "ID": id
    }), 200)

# Hosts on which experiments can be run
@app.route('/hosts', methods=['GET'])
def list_hosts():
    profiles = ['local']
    remote_profiles = [profile[0] for profile in list_profiles()]
    profiles.extend(remote_profiles)
    return make_response(
        jsonify(profiles), 
    200)

if __name__ == '__main__':
    print("Starting...")
    app.run(host=task._taskServerConfig.FLASK_HOST, port=task._taskServerConfig.FLASK_PORT)