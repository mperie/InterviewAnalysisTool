CREATE TABLE tasks (
	id serial PRIMARY KEY,
	corpus_id VARCHAR (255) NOT NULL,
	name VARCHAR (255) NOT NULL,
	type VARCHAR (255) NOT NULL,
	description VARCHAR (8000),
	created_on TIMESTAMP NOT NULL,
	progress INT NOT NULL,
	task_status INT NOT NULL,
	config jsonb NOT NULL,
	archived BOOLEAN NOT NULL,
	remote_run_id VARCHAR (255)
);

CREATE TABLE logs (
	id serial PRIMARY KEY,
	task_id INT NOT NULL,
	name VARCHAR (255) NOT NULL,
	error_message VARCHAR (8000) NOT NULL,
	FOREIGN KEY (task_id)
    	REFERENCES tasks (id)
);

CREATE TABLE preparing_logs(
	id serial PRIMARY KEY,
	task_id INT NOT NULL,
	message VARCHAR (8000) NOT NULL,
	FOREIGN KEY (task_id)
    	REFERENCES tasks (id)
);