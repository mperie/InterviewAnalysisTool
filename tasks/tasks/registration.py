from tasks.transcription import Transcription
from tasks.analyze import Analyze

Registration = {
    "tasks":{
        "transcription": Transcription,
        "analyze": Analyze
    }
}