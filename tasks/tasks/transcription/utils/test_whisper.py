from pyannote.audio import Pipeline # if you need to download the pyannote pipeline keep this import here (because of torch error)
# import whisper
import torch
import torchaudio
import librosa
import pickle
import dill
import json
import os
import tqdm

# load the pipeline from Hugginface Hub
# from pyannote.audio import Pipeline
# from pyannote.audio import Model
import os
import logging


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

def getSpeakerWithTimer(audioPath, num_speakers):
    with open("{}/pyannote-segmentation-pipeline.pickle".format(__location__), 'rb') as out_file:
        pipeline = pickle.load(out_file)
    # sauvegarde une version de la pipeline
    #pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization@2.1", use_auth_token="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")
    #with open("pyannote-segmentation-pipeline.pickle", "wb") as out_file:
    #    dill.dump(pipeline, out_file)

    diarization = pipeline(audioPath, num_speakers=num_speakers)

    listSegments = list(diarization._tracks)
    listSpeakers =list(map(lambda x: list(x.values())[0],list(diarization._tracks.values())))
    listSpeakerTimer = []
    for i in range(0, len(listSegments)):
        listSpeakerTimer.append({"speaker": listSpeakers[i], "start":list(diarization._tracks)[i].start, "end":list(diarization._tracks)[i].end, "text":""})

    return listSpeakerTimer

def millisec(timeStr):
  spl = timeStr.split(":")
  s = (int)((int(spl[0]) * 60 * 60 + int(spl[1]) * 60 + float(spl[2]) )* 1000)
  return s

def preparingAudioAfterDiarization(audioPath, speakerTimer):
    from pydub import AudioSegment
    import re 

    audio = AudioSegment.from_wav(audioPath)
    spacer = AudioSegment.silent(duration=2000)
    sounds = spacer
    segments = []

    for speak in speakerTimer:
        start = speak["start"]
        end = speak["end"]
        start = int(start*1000) #milliseconds
        end = int(end*1000)  #milliseconds
        
        segments.append(len(sounds))
        sounds = sounds.append(audio[start:end], crossfade=0)
        sounds = sounds.append(spacer, crossfade=0)

    sounds.export(audioPath, format="wav") #Exports to a wav file in the current path.


def getTextWithTimer(audioPath):
    if torch.cuda.is_available():
        with open(__location__+"/whisper-model-gpu.pickle", "rb") as model_file:
            model = dill.load(model_file)
        model = model.cuda(device=0)
    else:
        with open(__location__+"/whisper-model.pickle", "rb") as model_file:
            model = dill.load(model_file)
    # model = whisper.load_model("large")#.cuda(device=0)
    result = model.transcribe(audioPath, language="fr")
    for i in range(0, len(result["segments"])):
        result["segments"][i]["text"] = result["segments"][i]["text"].replace("''", "'")
    return result["text"].replace("''", "'"), result["segments"]

def dispatchSpeaker(listSpeakerTimer):
    i= 0
    while i < len(listSpeakerTimer)-1:
        if listSpeakerTimer[i]["speaker"] == listSpeakerTimer[i+1]["speaker"]:
            listSpeakerTimer[i]["end"] = max(listSpeakerTimer[i]["end"],listSpeakerTimer[i+1]["end"])
            del listSpeakerTimer[i+1]
        elif listSpeakerTimer[i]["end"] > listSpeakerTimer[i+1]["start"]:
            listSpeakerTimer[i]["speaker"] += " + {}".format(listSpeakerTimer[i+1]["speaker"])
            listSpeakerTimer[i]["end"] = max(listSpeakerTimer[i]["end"],listSpeakerTimer[i+1]["end"])
            del listSpeakerTimer[i+1]
        else:
            i = i + 1
    for i in range(0, len(listSpeakerTimer)-1):
        listSpeakerTimer[i]["end"] = listSpeakerTimer[i+1]["start"]
    return listSpeakerTimer

def fromAudioToText(audioPath, textPath, num_speakers=4):
    logging.info("Getting speakers...")
    speakerTimer = getSpeakerWithTimer(audioPath, num_speakers)
    logging.info("Preparing after diarization...")
    preparingAudioAfterDiarization(audioPath, speakerTimer)
    logging.info("Transcribing...")
    wholeText, textTimer = getTextWithTimer(audioPath)
    logging.info("Finished! Exporting results...")
    for i in range(len(textTimer)):
        del textTimer[i]["tokens"]
        del textTimer[i]["id"]
        del textTimer[i]["seek"]
        del textTimer[i]["temperature"]
        del textTimer[i]["avg_logprob"]
        del textTimer[i]["no_speech_prob"]
        del textTimer[i]["compression_ratio"]
        textTimer[i]["speaker"] = "speaker"+str(i%2)
    
    cleanJson = {
        "speakers":["speaker0", "speaker1"],
        "repliques": textTimer
    }
    with open(textPath, 'w') as f:
        json.dump(cleanJson, f)

    
    # with open(textPath, 'w') as f:
    #     for segment in textTimer:
    #         f.write(segment["text"])
    #         f.write("\n")


if __name__ == "__main__":
    audioPath = "/home/mperie/isabeau-data-audio/1min.wav"
