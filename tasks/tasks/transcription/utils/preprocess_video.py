import os
from moviepy.editor import *
from pydub import AudioSegment
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import librosa

import numpy as np
import soundfile as sf

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

def fromMp42Mp3(mp4Path, mp4Name, mp3Path, mp3Name):
    video = VideoFileClip(os.path.join(mp4Path,mp4Name))
    video.audio.write_audiofile(os.path.join(mp3Path, mp3Name))

def changeAudioFormat(audioPath, audioName, newAudioPath, newAudioame):
    newAudio = AudioSegment.from_file(os.path.join(audioPath, audioName))
    newAudio.export(os.path.join(newAudioPath, newAudioame), format="wav")

def addBlankAtStartOfEachAudio(id):
    audioDirectory = __location__+"/../audio/"+str(id)
    audios = os.listdir(audioDirectory)
    for audioName in audios:
        audio = AudioSegment.from_wav(audioDirectory+"/"+audioName)
        spacermilli = 2000
        spacer = AudioSegment.silent(duration=spacermilli)
        audio = spacer.append(audio, crossfade=0)
        audio.export(audioDirectory+"/"+audioName, format='wav')

# def cutVideo(t1, t2, mp4Path, mp4Name, newMp4Path, newMp4Name):
#     ffmpeg_extract_subclip("{}/{}".format(mp4Path,mp4Name), t1, t2, "{}/{}".format(newMp4Path,newMp4Name))

# def cutAudio(t1, t2, mp3Path, mp3Name, newMp3Path, newMp3Name):
#     t1 = t1 * 1000 #Works in milliseconds
#     t2 = t2 * 1000
#     newAudio = AudioSegment.from_file(os.path.join(mp3Path, mp3Name))
#     newAudio = newAudio[t1:t2]
#     newAudio.export(os.path.join(newMp3Path, newMp3Name), format="wav")

# def changeSampleRate(filePath, sampleRate=16000):
#     y, s = librosa.load(filePath, sr=sampleRate)
#     print(y, s)
#     sf.write(filePath, y, sampleRate, 'PCM_24')