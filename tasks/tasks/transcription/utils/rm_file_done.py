import os

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

def rmAudioFileDone():
    audioFiles = os.listdir(__location__+"/../audio/")
    textFiles = os.listdir(__location__+"/../text/")
    audioDirectory = __location__+"/../audio"
    textDirectory = __location__+"/../text"
    for text in textFiles:
        os.remove("{}/{}".format(audioDirectory,text+".wav"))

if __name__ == "__main__":
    rmAudioFileDone()
