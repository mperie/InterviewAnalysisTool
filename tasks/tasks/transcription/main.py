import os
import re
import shutil
from utils import *

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


def createAudioForEachVideo():
    videos = os.listdir(__location__+"/video")
    audioDirectory = "{}/{}".format(__location__, "audio")
    if os.path.exists(audioDirectory) and os.path.isdir(audioDirectory):
        shutil.rmtree(audioDirectory)
    os.makedirs(audioDirectory)
    for video in videos:
        fromMp42Mp3(__location__+"/video", video, audioDirectory, video.split(".")[0]+".wav")

def createAudioForEachAudio():
    audios = os.listdir(__location__+"/audio")
    audioDirectory = "{}/{}".format(__location__, "audio")
    if os.path.exists(audioDirectory) and os.path.isdir(audioDirectory):
        for audio in audios:
            changeAudioFormat(audioDirectory, audio, audioDirectory, re.sub(r"\s+", '_', audio.split(".")[0]+".wav"))
            os.remove("{}/{}".format(audioDirectory,audio))

def createTextForEachAudio(num_speakers):
    audioFiles = os.listdir(__location__+"/audio/")
    print(audioFiles)
    for audioFile in audioFiles:
        audioPath = __location__+"/audio/"+audioFile
        textSubFolder = __location__+"/text/"+(audioFile.split(".")[0])+"/"
        textPath = textSubFolder +(audioFile.split(".")[0])+".json"
        if os.path.exists(textSubFolder) and os.path.isdir(textSubFolder):
            shutil.rmtree(textSubFolder)
        os.makedirs(textSubFolder)
        fromAudioToText(audioPath, textPath, num_speakers=num_speakers)       

if __name__ == "__main__":
    input_data_is_video = False
    input_data_is_audio = True
    input_data_is_wav = False
    if input_data_is_video:
        createAudioForEachVideo()
    if input_data_is_audio:
        if not input_data_is_wav:
            createAudioForEachAudio()
    addBlankAtStartOfEachAudio() #else pyannote bug
    createTextForEachAudio(2)
    
