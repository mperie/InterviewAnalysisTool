import os
import re
import shutil
import argparse
import json
import sys

dir_path = os.path.dirname(os.path.realpath(__file__))
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(os.path.join(__location__, "../"))

from transcription.utils import fromMp42Mp3, changeAudioFormat, fromAudioToText, addBlankAtStartOfEachAudio
from callbacks import setDefaultCallbacks


class Transcription():

    def __init__(self, id, parameters, callbacks=None):
        self.id = id
        self.input_data_is_video = False
        self.input_data_is_audio = True
        self.input_data_is_wav = False
        if(callbacks == None):
            self.callbacks = setDefaultCallbacks(self.id)
        else:
            self.callbacks = callbacks

    def createAudioForEachMedia(self):
        mediaDirectory = __location__+"/media_files/"+ str(self.id)
        medias = os.listdir(mediaDirectory)
        audioDirectory = "{}/{}/{}".format(__location__, "audio", self.id)
        if os.path.exists(audioDirectory) and os.path.isdir(audioDirectory):
            shutil.rmtree(audioDirectory)
        os.makedirs(audioDirectory)
        for media in medias:
            if media.split(".")[1] == "mp3" or media.split(".")[1] == "wav":
                changeAudioFormat(mediaDirectory, media, audioDirectory, re.sub(r"\s+", '_', media.split(".")[0]+".wav"))
            elif media.split(".")[1] == "mp4":
                fromMp42Mp3(mediaDirectory, media, audioDirectory, media.split(".")[0]+".wav")



    def createAudioForEachVideo(self):
        videos = os.listdir(__location__+"/video")
        audioDirectory = "{}/{}".format(__location__, "audio")
        if os.path.exists(audioDirectory) and os.path.isdir(audioDirectory):
            shutil.rmtree(audioDirectory)
        os.makedirs(audioDirectory)
        for video in videos:
            fromMp42Mp3(__location__+"/video", video, audioDirectory, video.split(".")[0]+".wav")

    def createAudioForEachAudio(self):
        audios = os.listdir(__location__+"/audio")
        audioDirectory = "{}/{}".format(__location__, "audio")
        if os.path.exists(audioDirectory) and os.path.isdir(audioDirectory):
            for audio in audios:
                changeAudioFormat(audioDirectory, audio, audioDirectory, re.sub(r"\s+", '_', audio.split(".")[0]+".wav"))
                os.remove("{}/{}".format(audioDirectory,audio))

    def createTextForEachAudio(self, num_speakers):
        audioDirectory = "{}/{}/{}".format(__location__, "audio", self.id)
        audioFiles = os.listdir(audioDirectory)
        print(audioFiles)
        for audioFile in audioFiles:
            audioPath = audioDirectory+"/"+audioFile
            textSubFolder = __location__+"/text/"+str(self.id)+"/"+(audioFile.split(".")[0])+"/"
            textPath = textSubFolder +(audioFile.split(".")[0])+".json"
            if os.path.exists(textSubFolder) and os.path.isdir(textSubFolder):
                shutil.rmtree(textSubFolder)
            os.makedirs(textSubFolder)
            fromAudioToText(audioPath, textPath, num_speakers=num_speakers)
            self.callbacks["save"](textPath, "text_files")

    def start(self, **kwargs):
        self.createAudioForEachMedia()
        addBlankAtStartOfEachAudio(self.id) #else pyannote bug
        self.createTextForEachAudio(3)
        self.callbacks["finished"]()
        print("transcription task end")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_file', type=str, required=True)
    parser.add_argument('--task_id', type=int, required=True)
    
    args = parser.parse_args()
    
    with open(args.config_file) as json_file:
        config = json.load(json_file) 

    transcription = Transcription(args.task_id, config)
    transcription.start()