from typing import Callable, Dict, List
from tasks import Registration

def create(parameters: Dict, task_id: int, callbacks: dict):
    task_type = parameters["task"].pop("type")
    task = Registration["tasks"][task_type](task_id, parameters, callbacks=callbacks)
    return task

def start(task, **kwargs):
    task.start(kwargs=kwargs)