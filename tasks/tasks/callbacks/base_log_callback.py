from typing import List
import os
from callbacks import BaseCallback

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

class BaseLogFileCallback(BaseCallback):
    '''
    Base class for on discovery callbacks used by the experiment pipelines when new discovery is made.
    '''

    def __init__(self, task_id, **kwargs) -> None:
        """
            initialize attributes common to all on discovery callbacks

            Args:
                to_save_outputs: Names of all outputs the user want save
                kwargs: some usefull args (e.g. experiment_id...)
        """
        super().__init__(**kwargs)
        self.task_id = task_id
        self.logFile = __location__ + "/../logs/{}.log".format(self.task_id)
        self.log_index=0


    def __call__(self, **kwargs) -> None:
        """
            The function to call to effectively raise on discovery callback.
            Inform the user that the experiment as made new discovery
            Args:
                task_id: current experiment id
                seed: current seed number
                kwargs: somme usefull parameters
        """
        
        self.logId = "{}_{}_{}".format(self.task_id, self.logType, self.log_index)
        self.log_index+=1