from typing import List
import os
from callbacks import BaseLogFileCallback

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

class LogTaskFinished(BaseLogFileCallback):
    '''
    Base class for on discovery callbacks used by the experiment pipelines when new discovery is made.
    '''

    def __init__(self, **kwargs) -> None:
        """
            initialize attributes common to all on discovery callbacks

            Args:
                to_save_outputs: Names of all outputs the user want save
                kwargs: some usefull args (e.g. experiment_id...)
        """
        super().__init__(**kwargs)
        self.logType = "taskFinished"


    def __call__(self, **kwargs) -> None:
        """
            The function to call to effectively raise on discovery callback.
            Inform the user that the experiment as made new discovery
            Args:
                task_id: current experiment id
                seed: current seed number
                kwargs: somme usefull parameters
        """
        super().__call__(**kwargs)
        with open(self.logFile, "a") as file:
            file.write("IAnalyser - TASK FINISHED - TASK_ID {} - LOG_ID {}\n".format(self.task_id, self.logId))