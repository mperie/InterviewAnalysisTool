from callbacks import LogFileOnDisk, LogTaskFinished
def setDefaultCallbacks(task_id):
    return {
            "save": LogFileOnDisk(task_id=task_id),
            "finished": LogTaskFinished(task_id=task_id),
            "error": None
        }