from typing import List
import os
from callbacks import BaseLogFileCallback

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

class LogFileOnDisk(BaseLogFileCallback):
    '''
    Base class for on discovery callbacks used by the experiment pipelines when new discovery is made.
    '''

    def __init__(self, **kwargs) -> None:
        """
            initialize attributes common to all on discovery callbacks

            Args:
                to_save_outputs: Names of all outputs the user want save
                kwargs: some usefull args (e.g. experiment_id...)
        """
        super().__init__(**kwargs)
        self.logType = "fileondisk"


    def __call__(self, file_path:str, output_type: str, **kwargs) -> None:
        """
            The function to call to effectively raise on discovery callback.
            Inform the user that the experiment as made new discovery
            Args:
                task_id: current experiment id
                seed: current seed number
                kwargs: somme usefull parameters
        """
        super().__call__(**kwargs)
        with open(self.logFile, "a") as file:
            file.write("IAnalyser - NEW FILE - TASK_ID {} - LOG_ID {} - OUTPUT_TYPE {} - FILE_PATH {} \n".format(self.task_id, self.logId, output_type, file_path))