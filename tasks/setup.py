from setuptools import setup
import sys

assert sys.version_info.major == 3 and sys.version_info.minor in [10] , \
    "This repo is designed to work with Python 3.10" \
    + "Please install it before proceeding."

setup(
    name='tasks',
    py_modules=['tasks'],
    version="1.0",
    install_requires=[
        'addict==2.4.0',
        'matplotlib==3.5.3',
        'numpy==1.21.6',
        'pillow==9.2.0',
        'requests==2.28.1',
        'torch==1.12.1',
        'torchaudio==0.12.1',
        'librosa==0.9.2',
        'dill==0.3.5.1',
        'imageio==2.22.0',
        'imageio-ffmpeg==0.4.7',
        'huggingface-hub==0.12.0',
        'whisper @ git+https://github.com/openai/whisper.git@02b74308fff49aa0d5dd603faefa76d2edd8d56b',
        'moviepy==1.0.3',
        'pydub==0.25.1',
        'pyannote.audio==2.1.1'
    ],
    description="tasks Python lib",
    author="Flowers Team Inria",
)